mod app;
mod components;
use app::App;

#[cfg(not(target_arch = "wasm32"))]
#[tokio::main(flavor = "multi_thread", worker_threads = 10)]
async fn main() -> eframe::Result<()> {
    tracing_subscriber::fmt::init(); // Log to stdout (if you run with 'RUST_LOG=debug')

    let native_options = eframe::NativeOptions { // TODO schau dir die Options an
        initial_window_size: Some(egui::vec2(320., 240.)),
        ..Default::default()
    };

    eframe::run_native(
        "egui_demo",
        native_options,
        Box::new(|_cc| Box::new(App::new())),
    )
}

#[cfg(target_arch = "wasm32")]
#[tokio::main(flavor = "current_thread")]
async fn main() {
    console_error_panic_hook::set_once(); // Make sure panics are logged using 'console.error'.
    tracing_wasm::set_as_global_default(); // Redirect tracing to console.log and friends:

    let web_options = eframe::WebOptions::default(); // TODO auch hier was ist alles möglich

    wasm_bindgen_futures::spawn_local(async {
        eframe::start_web(
            "app",
            web_options,
            Box::new(|_cc| Box::new(App::new())),
        )
        .await
        .expect("Failed to start egui_demo");
    });
}
