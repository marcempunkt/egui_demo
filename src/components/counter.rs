// use std::sync::{Arc, MutexGuard};
use std::rc::Rc;
use std::cell::RefMut;
use egui::Ui;
use crate::app::AppContext;

pub struct Counter {
    app_context: Rc<AppContext>,
    value: i64,
}

impl Counter {
    pub fn new(app_context: Rc<AppContext>) -> Self {
        Self {
            app_context,
            value: 0,
        }
    }

    pub fn render(&mut self, ui: &mut Ui) {
        let mut api_url: RefMut<String> = self.app_context.api.api_url.borrow_mut();

        ui.horizontal(|ui: &mut Ui| {
            ui.label(&*api_url);

            if ui.button("update").clicked() {
                *api_url = self.value.to_string();
            }
        });

        ui.horizontal(|ui: &mut Ui| {
            if ui.button("-").clicked() {
                self.value -= 1;
            }

            ui.label(self.value.to_string());

            if ui.button("+").clicked() {
                self.value += 1;
            }
        });
    }
}
