use tokio::sync::oneshot::{channel, error, Sender, Receiver};
use std::rc::Rc;
use egui::{Context, Ui};

pub struct AsyncButton {
    times: u64,
    future_rx: Rc<Option<Receiver<u64>>>,
}

impl AsyncButton {
    pub fn new() -> Self {
        AsyncButton {
            times: 0,
            future_rx: Rc::new(None),
        }
    }

    pub fn render(&mut self, ctx: &Context, ui: &mut Ui) {
        let future_rx: &mut Option<Receiver<u64>> = Rc::get_mut(&mut self.future_rx).unwrap();

        if let Some(ref mut rx) = future_rx {
            match rx.try_recv() {
                Ok(value) => self.times = value,
                Err(error::TryRecvError::Closed) => *future_rx = None,
                Err(error::TryRecvError::Empty) => (),
            };
        }

        ui.label(format!("future done {} times", self.times));

        if ui.button("fetch").clicked() {
            self.on_click(ctx);
        }
    }

    pub fn on_click(&mut self, ctx: &Context) {
        if self.future_rx.is_some() { return; }
        let ctx = ctx.clone();
        let times  = self.times;
        let (tx, rx): (Sender<u64>, Receiver<u64>) = channel::<u64>();
        tokio::spawn(async move {
            tokio::time::sleep(tokio::time::Duration::from_secs(2)).await;
            match tx.send(times + 1) {
                Ok(_) => ctx.request_repaint(),
                Err(_) => eprintln!("ERROR: Receiver has been dropped"),
            }
        });
        self.future_rx = Rc::new(Some(rx));
    }
}
