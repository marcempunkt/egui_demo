// use std::sync::{Arc, Mutex};
use std::rc::Rc;
use std::cell::RefCell;
use eframe::Frame;
use egui::{Context, Ui};
use crate::components::{async_button::AsyncButton, counter::Counter};

pub struct App {
    pub app_context: Rc<AppContext>,
    components: Components,
}

pub struct AppContext {
    pub api: ApiContext,
}

pub struct ApiContext {
    pub api_url: RefCell<String>,
}

struct Components {
    counter: Counter,
    async_button: AsyncButton,
}

impl App {
    pub fn new() -> Self {
        let app_context = Rc::new(AppContext{
            api: ApiContext {
                api_url: RefCell::new("https://maeurer.dev".to_string()),
            }
        });
        Self {
            app_context: Rc::clone(&app_context),
            components: Components {
                counter: Counter::new(Rc::clone(&app_context)),
                async_button: AsyncButton::new(),
            },
        }
    }
}

impl eframe::App for App {
    fn update(&mut self, ctx: &Context, _: &mut Frame) {
        egui::CentralPanel::default().show(ctx, |ui: &mut Ui| {
            ui.heading("My demo app");
            self.components.counter.render(ui);
            self.components.async_button.render(ctx, ui);
        });
    }
}
